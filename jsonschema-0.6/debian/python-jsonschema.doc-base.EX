Document: python-jsonschema
Title: Debian python-jsonschema Manual
Author: <insert document author here>
Abstract: This manual describes what python-jsonschema is
 and how it can be used to
 manage online manuals on Debian systems.
Section: unknown

Format: debiandoc-sgml
Files: /usr/share/doc/python-jsonschema/python-jsonschema.sgml.gz

Format: postscript
Files: /usr/share/doc/python-jsonschema/python-jsonschema.ps.gz

Format: text
Files: /usr/share/doc/python-jsonschema/python-jsonschema.text.gz

Format: HTML
Index: /usr/share/doc/python-jsonschema/html/index.html
Files: /usr/share/doc/python-jsonschema/html/*.html
