-----BEGIN PGP SIGNED MESSAGE-----
Hash: SHA1

Format: 3.0 (quilt)
Source: python-jsonschema
Binary: python-jsonschema
Architecture: all
Version: 0.6-1
Maintainer: Kristina Clair <kclair@leap.se>
Homepage: http://pypi.python.org/pypi/jsonschema
Standards-Version: 3.9.4.0
Build-Depends: debhelper (>= 7.0.50~), python-support, python (>= 2.6)
Checksums-Sha1: 
 3d0fa792c0a0da3f40315496060e80006b63fa29 13779 python-jsonschema_0.6.orig.tar.gz
 1bb563facbeabded0d8cbac44b77e9abfbf1aee9 11232 python-jsonschema_0.6-1.debian.tar.gz
Checksums-Sha256: 
 e55d50467b3f1a813e62ab8d2d9d46a095ccc1238e9183e650b078359f5a9356 13779 python-jsonschema_0.6.orig.tar.gz
 e0244b54889641318d7837a000327e913e285afccb117eeb31d81aa383f40108 11232 python-jsonschema_0.6-1.debian.tar.gz
Files: 
 f48327683818b2db47611599c7c04cc0 13779 python-jsonschema_0.6.orig.tar.gz
 736a56cb3b019ed65e9a0f54a5a330bc 11232 python-jsonschema_0.6-1.debian.tar.gz

-----BEGIN PGP SIGNATURE-----
Version: GnuPG v1.4.10 (GNU/Linux)
Comment: GPGTools - http://gpgtools.org

iQIcBAEBAgAGBQJQbIWlAAoJEJ6m/xxX04Tjya0P/0S9lTXCbNfg1ile7hWoBaKx
2SlyVbE5TBDe70AcSX6bTd4gTQg18znUvjcWdjWGw0IPqSl2q5y20sXDodasXxlD
wpi/FVd6FBkfkT4AEndqZO9iQAR1mQkSCrJirgfsEYoLOYmlGRO2jdLrHj37O49P
BCc3J00OM4nRNRJ52c5C6e4HG0Y7Hpqd++ATMDsQFvJaTFFDqI/0VHWYHBvQJ2LK
yeIbGSE9wDyOxaaVtsN+NDuWfa3af3UdQ+m5uVSVrvXre8ghMzUqilrTUGI48QbD
/eBZ5aalqkOJDS0UHJ9QY0MeTAFWFhrCFq2CAGCx101MNUDGMl+KzqQDzCkoMjOH
PcZ+yHWN8HwK5Wwi2ceNIUKDX/018cJTeS/Jlwv0se6n+sdFNWcxDRHMPWuBUyXo
Gr/7/ZXLhsqmTD33VlUwc3byvnJPcSs9HWcY+vSOBLvFVzWm8tE+uYVGyK24ZZlB
vfF2duqnhEiNj/uWSnjZ6ypfVliK0dI6t/ew3118uEfge04bvRBXjNUk/cMYImna
dCm7ax4gnjQ/D8xmBjY62t1dBOnWNUmAY/Kne0Ut77XgHtc3aOD954lz+Vsd/gg0
IO4d93IFJ6vel1jcWm3+xIxuXisNmpiiJ1E/VODP5vo5ieuDIHPGnSiZ4Yc+Z2F4
j1FHoFq6HYntJMwTqTdr
=z7HP
-----END PGP SIGNATURE-----
